angular
  .controller('static_loginregisterCtrl', loadFunction);

loadFunction.$inject = ['$scope', 'structureService', '$location', 'FirebaseAuth'];

function loadFunction($scope, structureService, $location, FirebaseAuth){
  //Register upper level modules
  structureService.registerModule($location,$scope,"static_loginregister");
  var firebaseConfig = createConfig($scope.static_loginregister.modulescope);

  var auth           = FirebaseAuth.init(firebaseConfig);
  $scope.send        = sendData;
  $scope.buttonText  = 'login';

  function sendData() {
    var loginData = {
       email:    ($scope.static_loginregister.email) ? $scope.static_loginregister.email : '',
       password: ($scope.static_loginregister.pwd)   ? $scope.static_loginregister.pwd   : ''
    };
    auth.signInWithEmailAndPassword(loginData.email, loginData.password)
      .then(goToIndex)
      .catch(function(error) {
        if( error.code !== 'auth/user-not-found' ) {
          $scope.static_loginregister.status = error.message;
          applyScope();
        } else {
          $scope.buttonText = 'register';
          createuser(loginData);
        }
      });
  }

  function createuser(loginData) {
    auth.createUserWithEmailAndPassword(loginData.email, loginData.password)
      .then(goToIndex)
      .catch(function(error) {
        $scope.static_loginregister.status = error;
        applyScope();
      });
  }

  function goToIndex() {
    $location.path(structureService.get().config.indexOld);
    applyScope();
   }

  function createConfig(scope) {
    var config = {
      apiKey      : scope.config.apiKey,
      authDomain  : scope.config.projectId+'.firebaseapp.com',
      databaseURL : 'https://'+scope.config.databaseName+'.firebaseio.com'
    };
    return config;
  }

  function applyScope() {
    if(!$scope.$$phase) {
      $scope.$apply();
    }
  }

}
