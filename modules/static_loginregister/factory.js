angular.module('factoria', ['firebase'])
  .factory("FirebaseAuth", ["$firebaseAuth", function($firebaseAuth){
    var ref = {};
    return {
      init : init
    };
    function init(config) {
      if(firebase && firebase.apps.length == 0){
        firebase.initializeApp(config);
      }
      return firebase.auth();
    }
  }]);
