angular
  .module('king.services.simpleregisterlogin', [])
  .run(['configService', 'structureHooks', function(configService, structureHooks) {
    var config = {};
    var moduleFiles = {};
    var defaultUrl = '/login-register-XZ';
    var module = {};
    if (configService.services && configService.services.simpleregisterlogin) {
      console.log('[V] Loading login config...');
      config = configService.services.simpleregisterlogin.scope;
      moduleFiles = configService.services.simpleregisterlogin.module;
      load();
    }

    function load() {
      structureHooks.setIndex(defaultUrl);
      module[defaultUrl] = loginRegister();
      structureHooks.addModule(module);
    }

    function loginRegister() {
      return {
        name: 'Login / Register',
        identifier: 'static_loginregister',
        type: 'A',
        showOn: {
          menu: false,
          market: false,
          dragDrop: false
        },
        moduleFolder: moduleFiles.moduleFolder,
        view: moduleFiles.view,
        files: [moduleFiles.controller, moduleFiles.factory],
        libs: [{
          bower: {
            'firebase': '3.5.0'
          },
          src: 'https://www.gstatic.com/firebasejs/3.5.0/firebase.js'
        }, {
          bower: {
            'angularfire': '2.0.2'
          },
          src: 'https://cdn.firebase.com/libs/angularfire/2.0.2/angularfire.min.js'
        }],
        scope: {
          'config': config
        }
      };
    }
  }]);
